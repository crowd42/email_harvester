#!/usr/bin/env python
# -*- codng: UTF-8 -*-

import argparse
import re
import sys
from bs4 import BeautifulSoup
import requests

# email_harvester take on argument: -d "site:tld +@ gmail com"
parser = argparse.ArgumentParser(description="Extract Emails")
parser.add_argument('-d', '--dork')
args = parser.parse_args()
if not args.dork:
    parser.print_help()
    sys.exit(1)


def get_emails(link):
    """(url) -> file
    Extract email address from a web page."""
    try:
        src = requests.get(link)
        data = src.text
        emails = re.findall(r"[\w\.-]+@[\w\.-]+\.com", data)

        # Write the output in a file, the name starts with numbers of email in
        # "emails", for ex: 10_emails.txt, 210_emails.txt etc.
        with open("emails.txt", 'a') as f:
            for email in emails:
                f.write(email+"\n")
    except requests.exceptions.ConnectionError:
            print("oops")
    except requests.exceptions.InvalidSchema:
        print('re oops')


# We starts at the first page of Google's results.
next_page = 0
while True:
    # The parameters to pass in URL.
    payloads = {'q': args.dork,
                "start": next_page}

    # Specify the headers.
    headers = {'user-agent': 'my-app/0.0.1'}
    url = requests.get("http://google.fr/search",
                       params=payloads, headers=headers)
    links = url.links
    bs = BeautifulSoup(url.content, 'lxml')
    for i in bs.find_all('a'):
        # Filter-out unrelated links and extract actual URL from Google's.
        if i.get('href').startswith('/url?') and not i.get('href').startswith(
                "/url?q=http://webcache.googleusercontent.com"):
            extract = re.findall(r"q=(.*\.txt)", i.get('href'))
            for link in extract:
                get_emails(link)

    page = input("\n[*] Next page (Y/n?: )").lower()
    if page == "n":
        break
    else:
        # We move to the next 10 Google's results.
        next_page += 10
